Sample project for [Java BrowserID Verifier](https://github.com/user454322/browserid-verifier).

<br />

1. Clone it: `git clone https://github.com/user454322/browserid-verifier.git`
2. Run it: `cd browserid-verifier/sample/ && mvn jetty:run`
3. See it: go to [http://127.0.0.1:8080/](http://127.0.0.1:8080/)

<br />

Alternatively see a [live sample](https://browseridverifiersample-user454322.rhcloud.com/) in OpenShift.

